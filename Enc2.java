import java.io.IOException;
import java.util.*;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.conf.*;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.*;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import javax.crypto.SecretKey;
import javax.crypto.Cipher;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.InvalidKeyException;
import java.security.InvalidAlgorithmParameterException;

import javax.crypto.NoSuchPaddingException;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;

import sun.misc.BASE64Encoder;

public class Enc2 {

	public static class Map extends Mapper<LongWritable, Text, LongWritable, Text> {
		private Text word = new Text();
		private KeyPair keyPair;

		public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {

			try {

				keyPair = KeyPairGenerator.getInstance("RSA").generateKeyPair();
				Cipher cipher = Cipher.getInstance("RSA");
				cipher.init(Cipher.ENCRYPT_MODE, keyPair.getPublic());

				byte[] encryptedBytes = cipher.doFinal(value.toString().getBytes());
				String chipertext = new String(Base64.getEncoder().encode(encryptedBytes));
				context.write(key, new Text(chipertext));
			} catch (NoSuchAlgorithmException noSuchAlgo) {
				System.out.println(" No Such Algorithm exists " + noSuchAlgo);
			}

			catch (NoSuchPaddingException noSuchPad) {
				System.out.println(" No Such Padding exists " + noSuchPad);
			}

			catch (InvalidKeyException invalidKey) {
				System.out.println(" Invalid Key " + invalidKey);
			}

			catch (BadPaddingException badPadding) {
				System.out.println(" Bad Padding " + badPadding);
			}

			catch (IllegalBlockSizeException illegalBlockSize) {
				System.out.println(" Illegal Block Size " + illegalBlockSize);
			}

		}
	}

	public static class IntSumReducer extends Reducer<Text, LongWritable, Text, LongWritable> {
		private LongWritable result = new LongWritable();

		public void reduce(LongWritable key, Text values, Context context) throws IOException, InterruptedException {

			String EncryptedData = values.toString();

			context.write(new Text(EncryptedData), key);

		}
	}

	public static void main(String[] args) throws Exception {
		Configuration conf = new Configuration();

		Job job = new Job(conf, "Enc2");
		job.setJarByClass(Enc2.class);

		job.setOutputKeyClass(LongWritable.class);
		job.setOutputValueClass(Text.class);

		job.setMapperClass(Map.class);
		job.setCombinerClass(IntSumReducer.class);
		job.setReducerClass(IntSumReducer.class);

		job.setInputFormatClass(TextInputFormat.class);
		job.setOutputFormatClass(TextOutputFormat.class);

		FileInputFormat.addInputPath(job, new Path(args[0]));
		FileOutputFormat.setOutputPath(job, new Path(args[1]));

		job.waitForCompletion(true);
	}
}
